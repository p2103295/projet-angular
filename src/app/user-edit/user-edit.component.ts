import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserModel} from "../models/user.model";
import {Subscription} from "rxjs";
import {UsersServiceService} from "../shared/services/users.service.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {WarningDialogComponent} from "../warning-dialog/warning-dialog.component";
import {DeleteConfirmationComponent} from "../delete-confirmation/delete-confirmation.component";

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit, OnDestroy {
  user: UserModel | null = null;
  subscriptions: Subscription[] = [];
  constructor(private userService: UsersServiceService, private route: ActivatedRoute, private router: Router, private noUserDialog: MatDialog, private deleteDialog: MatDialog, private snackBar: MatSnackBar) {

  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const id = params['id'];
      this.userService.selectUser(id);
      let subscriptionUser = this.userService.selectedUser.subscribe(
        (user) => {
          this.user = user;
        }
      );
      this.subscriptions.push(subscriptionUser);
    });
    if (this.user == null) {
      // this.openDialog();
    }
  }

  openDialog(): void {
    const dialogRef = this.noUserDialog.open(WarningDialogComponent, {
      width: '500px',
    });
  }

  ngOnDestroy(): void {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }

  updateUser() {
    if (!this.user) {
      return;
    }

    this.userService.majUser(String(this.user.id), this.user);
    this.snackBar.open("updated user", "ok");
    this.router.navigate(['/user-details', this.user.id])
  }

  deleteUser() {
    if (!this.user || !this.user.id) {
      return;
    } else {
      const dialogRef = this.deleteDialog.open(DeleteConfirmationComponent);
      dialogRef.afterClosed().subscribe(async res => {
        if (res) {
          this.userService.deleteUser(String(this.user!.id));
          await this.router.navigate(['/home']);
          this.snackBar.open("deleted user", "ok")
        } else {
          dialogRef.close();
        }
      });
    }
  }
}
