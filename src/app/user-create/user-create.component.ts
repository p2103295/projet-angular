import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserModel} from "../models/user.model";
import {UsersServiceService} from "../shared/services/users.service.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss']
})
export class UserCreateComponent implements OnDestroy {
  user: UserModel | null = null;
  subscriptions: Subscription[] = [];
  first_name = "";
  last_name = "";
  email = "";
  occupation = "";
  bio = "";
  constructor(private userService: UsersServiceService, private route: ActivatedRoute, private router: Router, private snackBar: MatSnackBar) {

  }
  ngOnDestroy(): void {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }

  onSubmit() {
    if (this.first_name === "") return;
    if (this.last_name === "") return;
    if (this.email === "") return;
    if (this.occupation === "") return;
    if (this.bio === "") return;
    let newUser = new UserModel();
    newUser.first_name = this.first_name;
    newUser.last_name = this.last_name;
    newUser.email = this.email;
    newUser.occupation = this.occupation;
    newUser.bio = this.bio;
    this.userService.addUser(newUser);
    this.userService.selectUserByName(this.first_name, this.last_name);
    let subscriptionUser = this.userService.selectedUser.subscribe(
      (user) => {
        this.user = user;
      }
    );
    this.subscriptions.push(subscriptionUser);
    this.snackBar.open("created user", "ok")
    this.router.navigate(['/user-details', this.user?.id]);
  }
}
