import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {UserDetailsComponent} from "./user-details/user-details.component";
import {UserEditComponent} from "./user-edit/user-edit.component";
import {UserCreateComponent} from "./user-create/user-create.component";

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch:"full" },
  { path: 'home', component: HomeComponent },
  { path: 'user-details/:id', component: UserDetailsComponent },
  { path: 'user-edit/:id', component: UserEditComponent },
  { path: 'user-create', component: UserCreateComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
