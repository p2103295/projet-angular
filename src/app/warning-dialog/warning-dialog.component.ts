import { Component } from '@angular/core';
import {UsersServiceService} from "../shared/services/users.service.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-warning-dialog',
  templateUrl: './warning-dialog.component.html',
  styleUrls: ['./warning-dialog.component.scss']
})
export class WarningDialogComponent {

  constructor(public dialogRef: MatDialogRef<WarningDialogComponent>, private router: Router) {

  }
  onClick(): void {
    this.dialogRef.close();
  }
}
