import {Injectable} from '@angular/core';
import {UserModel} from "../../models/user.model";
import {BehaviorSubject, catchError, throwError} from "rxjs";
import {HttpServiceService} from "./http-service.service";

@Injectable({
  providedIn: 'root'
})
export class UsersServiceService {

  selectedUser = new BehaviorSubject<UserModel | null>(null);
  users = new BehaviorSubject<UserModel[]>([]);

  constructor(private httpService: HttpServiceService) {
  }

  selectUser(id: number) {
    const sub = this.httpService
      .getUser(id)
      .pipe(
        catchError((error) => {
          this.selectedUser.next(null);
          return throwError(error);
        })
      )
      .subscribe((data) => {
        this.selectedUser.next(data);
        sub.unsubscribe();
      });
  }

  selectUserByName(firstName: string, lastName: string) {
    const sub = this.httpService
      .getUsers()
      .pipe(
        catchError((error) => {
          this.selectedUser.next(null);
          return throwError(error);
        })
      )
      .subscribe((users: UserModel[]) => {
        const filteredUser = users.find(user => user.first_name === firstName && user.last_name === lastName);

        if (filteredUser) {
          this.selectedUser.next(filteredUser);
        } else {
          this.selectedUser.next(null);
        }

        sub.unsubscribe();
      });
  }


  addUser(user: UserModel) {
    const sub = this.httpService
      .addUser(user)
      .subscribe((data) => {
        this.selectedUser.next(data);
        sub.unsubscribe();
      });
  }

  getUsers() {
    this.httpService
      .getUsers()
      .subscribe((data: UserModel[]) => {
        this.users.next(data);
      });
  }

  majUser(id: string, user: UserModel) {
    const sub = this.httpService
      .putUser(id, user)
      .subscribe((data) => {
        this.selectedUser.next(data);
        sub.unsubscribe();
      });
  }

  deleteUser(id: string) {
    const sub = this.httpService
      .deleteUser(id)
      .subscribe((data) => {
        this.httpService.getUsers().subscribe((users) => {
          this.users.next(users);
        });
      });
  }
}
