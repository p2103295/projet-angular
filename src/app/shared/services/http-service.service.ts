import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {UserModel} from "../../models/user.model";

@Injectable({
  providedIn: 'root'
})
export class HttpServiceService {

  constructor(private httpClient: HttpClient) { }

  baseUrl = "https://658088e76ae0629a3f55694b.mockapi.io/gestion-users/users";

  public getUsers(): Observable<UserModel[]> {
    return this.httpClient.get<UserModel[]>(this.baseUrl);
  }

  public getUser(id: number): Observable<UserModel> {
    return this.httpClient.get<UserModel>(`${this.baseUrl}/${id}`);
  }

  public addUser(user: UserModel): Observable<UserModel> {
    return this.httpClient.post<UserModel>(`${this.baseUrl}`, user);
  }

  public deleteUser(id: string): Observable<any> {
    return this.httpClient.delete(`${this.baseUrl}/${id}`, {
      responseType: 'text',
    });
  }

  public putUser(id: string, user: UserModel): Observable<any> {
    return this.httpClient.put(`${this.baseUrl}/${id}`, user);
  }
}
