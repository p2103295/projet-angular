import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {MatSort} from "@angular/material/sort";
import { AfterViewInit } from '@angular/core';
import {Subscription} from "rxjs";
import {UserModel} from "../models/user.model";
import {UsersServiceService} from "../shared/services/users.service.service";
import {DeleteConfirmationComponent} from "../delete-confirmation/delete-confirmation.component";
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";


@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
})
export class UsersListComponent implements AfterViewInit, OnDestroy, OnInit {
    displayedColumns: string[] = ['id', 'first_name', 'last_name', 'occupation', 'buttons'];
    dataSource: MatTableDataSource<UserModel>;

    users: UserModel[] = [];
    subscriptions: Subscription[] = [];

     @ViewChild(MatPaginator) paginator!: MatPaginator;
     @ViewChild(MatSort) sort!: MatSort;

    constructor(private userService: UsersServiceService, private router: Router, private deleteDialog: MatDialog, private snackBar: MatSnackBar) {
        this.dataSource = new MatTableDataSource(this.users);
    }

    ngOnInit() {
        this.userService.getUsers();
        let subscriptionUser = this.userService.users.subscribe(
            (users) => {
                this.users = users;
                this.dataSource.data = users;
            }
        );
        this.subscriptions.push(subscriptionUser);
    }

    ngOnDestroy(): void {
        for (const subscription of this.subscriptions) {
            subscription.unsubscribe();
        }
    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

  deleteUser(user: UserModel) {
    if (!user || !user.id) {
      return;
    } else {
      const dialogRef = this.deleteDialog.open(DeleteConfirmationComponent);
      dialogRef.afterClosed().subscribe(async res => {
        if (res) {
          this.userService.deleteUser(String(user!.id));
          this.snackBar.open("deleted user", "ok")
        } else {
          dialogRef.close();
        }
      });
    }
  }
}
