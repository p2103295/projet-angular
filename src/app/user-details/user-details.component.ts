import {Component, OnDestroy, OnInit} from '@angular/core';
import {UsersServiceService} from "../shared/services/users.service.service";
import {MatTableDataSource} from "@angular/material/table";
import {UserModel} from "../models/user.model";
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {WarningDialogComponent} from "../warning-dialog/warning-dialog.component";
import {DeleteConfirmationComponent} from "../delete-confirmation/delete-confirmation.component";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit, OnDestroy {

  user: UserModel | null = null;
  subscriptions: Subscription[] = [];
  constructor(private userService: UsersServiceService, private route: ActivatedRoute, private router: Router, private noUserDialog: MatDialog, private deleteDialog: MatDialog, private snackBar: MatSnackBar) {

  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const id = params['id'];
      this.userService.selectUser(id);
      let subscriptionUser = this.userService.selectedUser.subscribe(
        (user) => {
            this.user = user;
        }
      );
      this.subscriptions.push(subscriptionUser);
    });
    if (this.user == null) {
      // this.openDialog();
    }
  }

  openDialog(): void {
    const dialogRef = this.noUserDialog.open(WarningDialogComponent, {
      width: '500px',
    });
  }

  ngOnDestroy(): void {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }

  deleteUser() {
    if (!this.user || !this.user.id) {
      return;
    } else {
      const dialogRef = this.deleteDialog.open(DeleteConfirmationComponent);
      dialogRef.afterClosed().subscribe(async res => {
        if (res) {
          this.userService.deleteUser(String(this.user!.id));
          await this.router.navigate(['/home']);
          this.snackBar.open("deleted user", "ok")
        } else {
          dialogRef.close();
        }
      });
    }
  }

}
